FROM python:3.5
MAINTAINER LauLe "vanlau.le@gmail.com"
ENV PYTHONUNBUFFERED 1
RUN mkdir /backend
RUN mkdir /backend/public_html
WORKDIR /backend/public_html
ADD requirements.txt /backend/public_html/
RUN pip install -r requirements.txt
ADD ./backend /backend/public_html/
