# README #

This tutorial uses docker(+ django, + mysql, +phpmyadmin) tools to build an app which includes django in role "backend", reactjs app in role "frontend", react native in role "mobile", "backend" works with "frontend", "mobile" throught api "GraphQL". 
Code in this project is similar to git "https://levanlau@bitbucket.org/levanlau/docker-django-graphql-reactjs-mysql-phpmyadmin.git" but it has more mobile react-native

# Step 1:

Follow the instructions git "https://levanlau@bitbucket.org/levanlau/docker-django-graphql-reactjs-mysql-phpmyadmin.git" to complete backend(django, mysql, phpmyadmin)
and frontend(reactjs)

# Step 2:

- Install NodeJs, Npm, or Yarn

- Use Npm(Yarn) to install "create-react-native-app" package

- Alongside "backend" folder, use "create-react-native-app" to install the first ReactNative app named "mobile"

- After the installing finished, naviage inside mobile folder, example run : "yarn start" or "npm start" to start app

- After the app began, inside commandline window will show a few inital requirements for running the app on Ios and Android device, among them has 
a "QR code" image

- On your mobile devices, it should install "Expo" app

- Begin running Expo app and scan "QR code" image above, here you should see demo react native app

# Hope userful for you